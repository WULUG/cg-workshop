#include <iostream>

int main() {
  int nx = 1920;
  int ny = 1080;

  std::cout << "P3\n" << nx << " " << ny << "\n255\n";

  for (int i = ny-1; i >= 0; i--) {
    for(int i = 0; i < nx; i++) {
      float r = float(i)/float(nx);
      float g = float(i)/float(ny);
      float b = 0.5;
      int ir = int(255.12*r);
      int ig = int(255.24*g);
      int ib = int(255.36*b);
      std::cout << ir << " " << ig << " " << ib << "\n";
    }
  }

}
