# Computer Graphics Workshop

This repository contains example and code for our workshop.

## Week 1

The week_1 folder contains files for our first week. You need a C++ compiler to
compile, and imagemagick or gimp to display the files. If you got our VM image, you
don't need to install anything!

**COMPILE**

You can compile and view the image like this:
```bash 
g++ -O2 first_colors.cpp -o out
out >> output.ppm
display output.ppm
```

`>> redirects whatever output out generates into a file called output.ppm.`

`Display is an image viewing utility from ImageMagick and comes pre-installed with many linux distributions.`


**FILES**

The img_vids file contains the images and videos I have shown in the workshop.

## Week 2

The week_2 folder contains our files for our second week. 

**COMPILE**

You can compile and view the image like this:
```bash 
g++ -O2 main.cpp -o out
out >> output.ppm
display output.ppm
```

## Later on

The steps are same from week 2 onwards.

